var path = require('path');
var express = require('express');
var app = express();
var fs = require('fs');
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
const https = require('https');
var session =require('express-session');
app.use('/public', express.static(path.join(__dirname, '/public')));
app.use(session({
    secret: "Özel-Anahtar",
    resave: false,
    saveUninitialized: true
  }));

const options = {
  key: fs.readFileSync('Certificate/Key.pem'),
  cert: fs.readFileSync('Certificate/Cert.pem')
};

https.createServer(options,app, (req, res) => {
  res.writeHead(200);
  res.end(indexHtml);
}).listen(8000);

var indexHtml;

fs.readFile("index.html", "utf8", function (err, data) {
    if (err != null) {
        console.log("index file reading error process exiting the error is:" + err);
        setTimeout(process.exit(1), 10000);
    }
    else {
        indexHtml = data;
		}
});

app.use('/assets',express.static('assets'));
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

var server = app.listen(3000, function () {
    var host = 'localhost';
    var port = server.address().port;
    console.log('listening on http://'+host+':'+port+'/');
});

app.get('/', function (req, res) {
    res.send(indexHtml);
});
app.post('/login', function (req, res) {
  
        var username = req.body.username,
            password = req.body.password;
            MongoClient.connect(url, function(err, db) {
                if (err) throw err;
                var dbo = db.db("apartdb");
                var query = { username: username, password : password };
                dbo.collection("user").find(query).toArray(function(err, result) {
                  if (err) throw err;
                  if(result[0]!=undefined){
                  if(result[0].username==username||result[0].password==password){
                  res.send(result);
                  var sessData = req.session;
                  
                  sessData.someAttribute = "foo";
                  }
                  }
                  else
                    console.log("Değildir")
                 
                  
                 
                
                });
                db.close();
              });
});

app.post('/signup', function (req, res) {
  
        var username = req.body.username,
            password = req.body.password;
            MongoClient.connect(url, function(err, db) {
                if (err) throw err;
                var dbo = db.db("apartdb");
                var query = { username: username, password : password };
                dbo.collection("user").insert(query, function(err, result) {
                  if (err) throw err;
                  if(result[0]!=undefined){
                  if(result[0].username==username||result[0].password==password){
                  res.send(result);
                  var sessData = req.session;
                  
                  sessData.someAttribute = "foo";
                  }
                  }
                  else
                    console.log("Değildir")
                 
                  
                 
                
                });
                db.close();
              });
});
app.get('/bar', function(req, res, next) {
    var someAttribute = req.session.someAttribute;
    res.send(`This will print the attribute I set earlier: ${someAttribute}`);
  });

  app.use(session({ secret: 'this-is-a-secret-token', cookie: { maxAge: 60000 }}));
 
// Access the session as req.session
app.get('/session', function(req, res, next) {
  var sessData = req.session;
  sessData.someAttribute = "foo";
  res.send('Returning with some text');
});

